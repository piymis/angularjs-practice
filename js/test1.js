var app1 = angular.module('app1', []);

app1.controller('ctrl1', function($scope) {
    $scope.first = 1;
    $scope.second = 1;

    $scope.updateCalculation = function() {
        $scope.calculation = (+$scope.first + +$scope.second);
    };
});